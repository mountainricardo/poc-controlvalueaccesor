import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JointVentureVendorComponent } from './joint-venture-vendor.component';

describe('JointVentureVendorComponent', () => {
  let component: JointVentureVendorComponent;
  let fixture: ComponentFixture<JointVentureVendorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JointVentureVendorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JointVentureVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
