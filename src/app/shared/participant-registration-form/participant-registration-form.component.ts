import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-participant-registration-form',
  templateUrl: './participant-registration-form.component.html',
  styleUrls: ['./participant-registration-form.component.sass'],
})
export class ParticipantRegistrationFormComponent implements OnInit {
  constructor(readonly fb: FormBuilder) {}

  public singleVendorForm: FormGroup;
  public vendorTypeList = ['OPTION1', 'OPTION2'];

  ngOnInit(): void {
    this.initFormValues();
  }

  initFormValues() {
    this.singleVendorForm = this.fb.group({
      name: new FormControl('', [Validators.required,Validators.minLength(4)]),
      vendorType: new FormControl('', [Validators.required]),
    });
  }
}
