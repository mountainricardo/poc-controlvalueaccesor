import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParticipantRegistrationFormComponent } from './participant-registration-form/participant-registration-form.component';
import { JointVentureVendorComponent } from './joint-venture-vendor/joint-venture-vendor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

@NgModule({
  declarations: [
    ParticipantRegistrationFormComponent,
    JointVentureVendorComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InputsModule,
    DropDownsModule,
  ],
  exports:[
    ParticipantRegistrationFormComponent,
    JointVentureVendorComponent,
  ]
})
export class SharedModule {}
