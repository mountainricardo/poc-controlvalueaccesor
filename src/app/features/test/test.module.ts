import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterParticipantComponent } from './register-participant/register-participant.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [RegisterParticipantComponent],
  imports: [CommonModule, SharedModule],
  exports: [RegisterParticipantComponent],
})
export class TestModule {}
